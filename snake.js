var cnv = document.getElementById('snake');
var cc = cnv.getContext('2d');

var dir = 'r';
var length = 5;
var score = length - 5;
var speed = 90;
var xk, yk, keys, scn, gov;
var x = new Array(50);
var y = new Array(50);

x[0] = 500;
y[0] = 500;
keys = 0;
scn = 0;
gov = false;

for (i = 1; i < length; i++) {
  x[i] = x[i-1] - 10;
  y[i] = y[i-1];
}

xk = Math.floor(((Math.random() * 800)/10))*10;
yk = Math.floor(((Math.random() * 630)/10))*10;

cc.fillStyle = '#000000';
cc.fillRect(0, 0, cc.canvas.width, cc.canvas.height);

draw();
if (gov != true) {
  setInterval(runAll, speed);
  setInterval(function () { if (gov != true) {scn++} }, 1000);
}

function runAll () {
  move();
  draw();
  scramble();
  checkW();
}

function draw () {
  cc.fillStyle = '#01FF70';
  cc.fillRect(x[0], y[0], 10,10);
  for (i = 1; i < length; i++) {
    cc.fillRect(x[i], y[i], 10, 10);
  }
  cc.fillStyle = '#FFDC00';
  cc.fillRect(xk, yk, 10, 10);
}

function move () {
  switch (dir) {
    case 'r':
      x[0] = x[0] + 10;
      y[0] = y[0];
      cc.fillStyle = '#000000';
      cc.fillRect(x[0] - 10, y[0], 10,10);
      break;
    case 'l':
      x[0] = x[0] - 10;
      y[0] = y[0];
      cc.fillStyle = '#000000';
      cc.fillRect(x[0] + 10, y[0], 10,10);
      break;
    case 'u':
      x[0] = x[0];
      y[0] = y[0] - 10;
      cc.fillStyle = '#000000';
      cc.fillRect(x[0], y[0] + 10, 10,10);
      break;
    case 'd':
      x[0] = x[0];
      y[0] = y[0] + 10;
      cc.fillStyle = '#000000';
      cc.fillRect(x[0], y[0] - 10, 10,10);
      break;
    default:
      alert('something went wrong!');
      break;
  }
  for (i = length; i > 0; i--) {
    if (x[0] == x[i] && y[0] == y[i]) {
      gov = true;
      document.getElementById('container').innerHTML = '<span class="after"><h1>you lost!</h1> <br> you scored <b>' + score + '</b>,<br>played for <b>' + scn + '</b> seconds,<br>and pressed <b>' + keys + '</b> keys.</span>'
    }
  }
  for (i = length; i > 0; i--) {
    x[i] = x[i-1];
    y[i] = y[i-1];
    switch (dir) {
      case 'r':
        cc.fillStyle = '#000000';
        cc.fillRect(x[i-1], y[i-1], 10,10);
        break;
      case 'l':
        cc.fillStyle = '#000000';
        cc.fillRect(x[i-1], y[i-1], 10,10);
        break;
      case 'u':
        cc.fillStyle = '#000000';
        cc.fillRect(x[i-1], y[i-1], 10,10);
        break;
      case 'd':
        cc.fillStyle = '#000000';
        cc.fillRect(x[i-1], y[i-1], 10,10);
        break;
      default:
        alert('something went wrong!');
        break;
    }
  }
}

function scramble () {
  if (x[0] == xk && y[0] == yk) {
    length++;
    score = length - 5;
    document.getElementById('score').innerHTML = score;
    xk = Math.floor(((Math.random() * 800)/10))*10;
    yk = Math.floor(((Math.random() * 630)/10))*10;
  }
}

function checkW () {
  if (x[0] > cc.canvas.width || y[0] > cc.canvas.height || x[0] < 0 || y[0] < 0) {
    gov = true;
    document.getElementById('container').innerHTML = '<span class="after"><h1>you lost!</h1> <br> you scored <b>' + score + '</b>,<br>played for <b>' + scn + '</b> seconds,<br>and pressed <b>' + keys + '</b> keys.</span>'
  }
}

function setSpeed (sp) {
  if (sp) {
    speed += 10;
  } else {
    speed -= 10;
  }
}

window.onkeyup = function (e) {
  var key = e.keyCode ? e.keyCode : e.which;

  if (key == 38) {
    if (dir != 'd') {
      dir = 'u';
      keys++;
    }
  };
  if (key == 40) {
    if (dir != 'u') {
      dir = 'd';
      keys++;
    }
  };
  if (key == 37) {
    if (dir != 'r') {
      dir = 'l';
      keys++;
    }
  };
  if (key == 39) {
    if (dir != 'l') {
      dir = 'r';
      keys++;
    }
  };
};
